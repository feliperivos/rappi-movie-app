package co.feliperivera.rappimovies.data.entities

import androidx.room.Entity
import co.feliperivera.rappimovies.data.entities.Item

@Entity
data class Movie(


    val title: String,
    val original_title: String?,
    val popularity: Float,
    val release_date: String?,
    val poster_path: String?,
    val overview: String?,
    val backdrop_path: String?,
    val vote_average: Float,
    val original_language: String?,
    val status: String?,
    ) : Item()