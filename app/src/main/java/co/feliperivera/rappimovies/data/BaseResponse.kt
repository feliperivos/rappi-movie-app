package co.feliperivera.rappimovies.data

import co.feliperivera.rappimovies.data.entities.Item

data class BaseResponse<T>(val results: List<T>)
