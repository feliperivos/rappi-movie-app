package co.feliperivera.rappimovies.data.daos

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import co.feliperivera.rappimovies.data.daos.BaseDao
import co.feliperivera.rappimovies.data.entities.TV

@Dao
interface TVDao: BaseDao<TV> {

    @Query("SELECT * FROM tv WHERE id = :id")
    fun getTVDetails(id: Int): LiveData<TV>

    @Query("SELECT * FROM tv ORDER BY popularity DESC")
    fun getMostPopularTVSeries(): PagingSource<Int, TV>

    @Query("SELECT * FROM tv ORDER BY vote_average DESC")
    fun getTopRatedTVSeries(): PagingSource<Int, TV>

    @Query("DELETE FROM tv")
    override suspend fun clearAll()

}