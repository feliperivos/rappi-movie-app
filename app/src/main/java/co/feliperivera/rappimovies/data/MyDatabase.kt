package co.feliperivera.rappimovies.data

import androidx.room.Database
import androidx.room.RoomDatabase
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.daos.RemoteKeysDao
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.RemoteKeys
import co.feliperivera.rappimovies.data.entities.TV

@Database(entities = [Movie::class, TV::class, RemoteKeys::class],version = 1, exportSchema = false)
abstract class MyDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun tvDao(): TVDao
    abstract fun remoteKeysDao(): RemoteKeysDao
}