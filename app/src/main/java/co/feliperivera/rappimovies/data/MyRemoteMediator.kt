package co.feliperivera.rappimovies.data

import android.util.Log
import androidx.paging.*
import co.feliperivera.rappimovies.data.daos.BaseDao
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.daos.RemoteKeysDao
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.Item
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.RemoteKeys
import co.feliperivera.rappimovies.data.entities.TV
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class MyRemoteMediator<T : Item>(
        private val remoteKeyType: ListType,
        private val dao: BaseDao<T>,
        private val remoteKeysDao: RemoteKeysDao,
        private val webService: WebService
): RemoteMediator<Int, T>(){

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, T>
    ): MediatorResult {
        return try {
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: 1
                }
                LoadType.PREPEND -> {
                    val remoteKeys = getRemoteKeyForFirstItem(state)
                    val prevKey = remoteKeys?.prevKey ?: return MediatorResult.Success(endOfPaginationReached = false)
                    prevKey
                }

                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    val nextKey = remoteKeys?.nextKey ?: return MediatorResult.Success(endOfPaginationReached = false)
                    nextKey
                }
            }
            val response = when(remoteKeyType){
                ListType.POPULAR_TV -> {
                    webService.getPopularTvShows(page)
                }
                ListType.TOP_RATED_TV -> {
                    webService.getTopRatedTvShows(page)
                }
                ListType.POPULAR_MOVIE -> {
                    webService.getPopularMovies(page)
                }
                ListType.TOP_RATED_MOVIE -> {
                    webService.getTopRatedMovies(page)
                }
            }
            Log.d("response", response.toString())
            Log.d("type",remoteKeyType.toString())
            if (loadType == LoadType.REFRESH) {
                //dao.clearAll()
                //remoteKeysDao.clearRemoteKeys(remoteKeyType)
            }
            if(dao is MovieDao){
                Log.d("isMovie", "true")
                dao.insertAll(response.results as List<Movie>)
            }else if (dao is TVDao){
                dao.insertAll(response.results as List<TV>)
            }

            val prevKey = if (page == 1) null else page - 1
            val nextKey = page + 1
            val keys = response.results.map {
                RemoteKeys(type = remoteKeyType, remoteId = it.id, prevKey = prevKey, nextKey = nextKey)
            }
            remoteKeysDao.insertAll(keys)

            MediatorResult.Success(
                endOfPaginationReached = false
            )
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }

    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, T>): RemoteKeys? {
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
                ?.let { item ->
                    remoteKeysDao.remoteKeysHighestNext(remoteKeyType)
                    /*if(item is TV){
                        if(item.vote_average == 0.0f && remoteKeyType == ListType.TOP_RATED_TV){
                            remoteKeysDao.remoteKeysHighestNext(remoteKeyType)
                        }else{
                            remoteKeysDao.remoteKeysRepoId(item.id, remoteKeyType)
                        }
                    }else{
                        remoteKeysDao.remoteKeysRepoId(item.id, remoteKeyType)
                    }*/

                }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, T>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
                ?.let { item ->
                    remoteKeysDao.remoteKeysRepoId(item.id, remoteKeyType)
                }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
            state: PagingState<Int, T>
    ): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                remoteKeysDao.remoteKeysRepoId(id, remoteKeyType)
            }
        }
    }


}




