package co.feliperivera.rappimovies.data

enum class ListType {
    POPULAR_TV,
    TOP_RATED_TV,
    POPULAR_MOVIE,
    TOP_RATED_MOVIE
}