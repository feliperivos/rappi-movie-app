package co.feliperivera.rappimovies.data.repositories

import android.util.Log
import co.feliperivera.rappimovies.data.WebService
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val webService: WebService
){

    suspend fun searchMovies(query: String): List<Movie>{
        return try {
            val response = webService.searchMovie(query)
            response.results
        }catch (e: IOException) {
            Log.d("Error", e.toString())
            ArrayList<Movie>()
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
            ArrayList<Movie>()
        }
    }

    suspend fun searchTV(query: String): List<TV>{
        return try {
            val response = webService.searchTv(query)
            response.results
        }catch (e: IOException) {
            Log.d("Error", e.toString())
            ArrayList<TV>()
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
            ArrayList<TV>()
        }
    }
}