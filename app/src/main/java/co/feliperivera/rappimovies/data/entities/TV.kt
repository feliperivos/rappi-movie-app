package co.feliperivera.rappimovies.data.entities

import androidx.room.Entity
import co.feliperivera.rappimovies.data.entities.Item

@Entity
data class  TV(

    val name: String,
    val original_name: String?,
    val popularity: Float,
    val first_air_date: String?,
    val poster_path: String?,
    val overview: String?,
    val backdrop_path: String?,
    val vote_average: Float?,
    val original_language: String?,
    val status: String?,
    ) : Item()