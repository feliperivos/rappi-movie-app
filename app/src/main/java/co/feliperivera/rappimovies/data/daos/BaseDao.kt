package co.feliperivera.rappimovies.data.daos

import androidx.room.*
import co.feliperivera.rappimovies.data.entities.Item
import co.feliperivera.rappimovies.data.entities.Movie

interface BaseDao<T: Item> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<T>)

    @Delete
    suspend fun delete(vararg item: T)

    suspend fun clearAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(item: T)

}