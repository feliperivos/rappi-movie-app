package co.feliperivera.rappimovies.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.feliperivera.rappimovies.data.ListType
import co.feliperivera.rappimovies.data.entities.RemoteKeys

@Dao
interface RemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<RemoteKeys>)

    @Query("SELECT * FROM remote_keys WHERE remoteId = :repoId AND type = :type")
    suspend fun remoteKeysRepoId(repoId: Int, type: ListType): RemoteKeys?

    @Query("SELECT * FROM remote_keys WHERE type = :type ORDER BY nextKey DESC LIMIT 1")
    suspend fun remoteKeysHighestNext(type: ListType): RemoteKeys?

    @Query("DELETE FROM remote_keys WHERE type = :type")
    suspend fun clearRemoteKeys(type: ListType)
}
