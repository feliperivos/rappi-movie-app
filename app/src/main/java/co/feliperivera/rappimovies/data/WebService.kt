package co.feliperivera.rappimovies.data

import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WebService {

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(@Query("page") page: Int) : BaseResponse<Movie>

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int) : BaseResponse<Movie>

    @GET("tv/top_rated")
    suspend fun getTopRatedTvShows(@Query("page") page: Int) : BaseResponse<TV>

    @GET("tv/popular")
    suspend fun getPopularTvShows(@Query("page") page: Int) : BaseResponse<TV>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(@Path("movie_id") movieId: Int): Movie

    @GET("movie/{movie_id}/videos")
    suspend fun getMovieVideos(@Path("movie_id") movieId: Int): BaseResponse<Videos>

    @GET("tv/{tv_id}")
    suspend fun getTVDetails(@Path("tv_id") tvId: Int): TV

    @GET("tv/{tv_id}/videos")
    suspend fun getTVVideos(@Path("tv_id") tvId: Int): BaseResponse<Videos>

    @GET("search/movie?page=1")
    suspend fun searchMovie(@Query("query") query: String): BaseResponse<Movie>

    @GET("search/tv?page=1")
    suspend fun searchTv(@Query("query") query: String): BaseResponse<TV>





}