package co.feliperivera.rappimovies.data.repositories

import android.util.Log
import co.feliperivera.rappimovies.data.Videos
import co.feliperivera.rappimovies.data.WebService
import co.feliperivera.rappimovies.data.daos.MovieDao
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val webService: WebService
) {
    suspend fun getMovieDetails(movieId: Int) {
        try{
            val response = webService.getMovieDetail(movieId)
            movieDao.update(response)
            Log.d("response", response.toString())
        } catch (e: IOException) {
            Log.d("Error", e.toString())
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
        }

    }

    suspend fun getMovieVideoLink(movieId: Int) : List<Videos>{
        return try{
            val response = webService.getMovieVideos(movieId)
            response.results
        } catch (e: IOException) {
            Log.d("Error", e.toString())
            ArrayList<Videos>()
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
            ArrayList<Videos>()
        }
    }
}