package co.feliperivera.rappimovies.data.entities

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity
abstract class Item (
    @PrimaryKey
    var id: Int = 0,
)