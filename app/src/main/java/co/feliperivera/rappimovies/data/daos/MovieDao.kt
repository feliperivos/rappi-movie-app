package co.feliperivera.rappimovies.data.daos

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import co.feliperivera.rappimovies.data.daos.BaseDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV

@Dao
interface MovieDao : BaseDao<Movie> {

    @Query("SELECT * FROM movie WHERE id = :id")
    fun getMovieDetails(id: Int): LiveData<Movie>

    @Query("SELECT * FROM movie ORDER BY popularity DESC")
    fun getMostPopularMovies(): PagingSource<Int, Movie>

    @Query("SELECT * FROM movie ORDER BY vote_average DESC")
    fun getTopRatedMovies(): PagingSource<Int, Movie>

    @Query("DELETE FROM movie")
    override suspend fun clearAll()






}