package co.feliperivera.rappimovies.data.repositories

import android.util.Log
import co.feliperivera.rappimovies.data.Videos
import co.feliperivera.rappimovies.data.WebService
import co.feliperivera.rappimovies.data.daos.TVDao
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class TVRepository @Inject constructor(
    private val tvDao: TVDao,
    private val webService: WebService
) {
    suspend fun getTVDetails(tvId: Int) {
        try{
            val response = webService.getTVDetails(tvId)
            tvDao.update(response)
            Log.d("response", response.toString())
        } catch (e: IOException) {
            Log.d("Error", e.toString())
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
        }

    }

    suspend fun getTVVideoLink(tvId: Int) : List<Videos>{
        return try{
            val response = webService.getTVVideos(tvId)
            response.results
        } catch (e: IOException) {
            Log.d("Error", e.toString())
            ArrayList<Videos>()
        } catch (e: HttpException) {
            Log.d("Error", e.toString())
            ArrayList<Videos>()
        }
    }
}