package co.feliperivera.rappimovies.data

data class Videos(
    val key: String,
    val site: String
)
