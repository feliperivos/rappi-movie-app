package co.feliperivera.rappimovies.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import co.feliperivera.rappimovies.data.ListType

@Entity(tableName = "remote_keys")
data class RemoteKeys(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,
        val type: ListType,
        val remoteId: Int,
        val prevKey: Int?,
        val nextKey: Int?
        )



