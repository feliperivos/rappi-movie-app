package co.feliperivera.rappimovies.ui.search

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.feliperivera.rappimovies.R
import co.feliperivera.rappimovies.databinding.FragmentSearchBinding
import co.feliperivera.rappimovies.ui.movies.MoviesSearchRecyclerViewAdapter
import co.feliperivera.rappimovies.ui.tv.TVSearchRecyclerViewAdapter
import co.feliperivera.rappimovies.viewmodels.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment: Fragment() {

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!
    private val model: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        if (Intent.ACTION_SEARCH == requireActivity().intent.action) {
            requireActivity().intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                model.query = query
                model.findMovies()
                model.findTVShows()
                model.movies.observe(viewLifecycleOwner) {
                    val adapter = MoviesSearchRecyclerViewAdapter(it)
                    binding.movieList.layoutManager = LinearLayoutManager(context)
                    binding.movieList.adapter = adapter
                }
                model.tvShows.observe(viewLifecycleOwner) {
                    val adapter = TVSearchRecyclerViewAdapter(it)
                    binding.tvList.layoutManager = LinearLayoutManager(context)
                    binding.tvList.adapter = adapter
                }
            }
        }


        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}