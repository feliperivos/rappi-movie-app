package co.feliperivera.rappimovies.ui.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import co.feliperivera.rappimovies.R
import co.feliperivera.rappimovies.databinding.FragmentMoviesBinding
import co.feliperivera.rappimovies.databinding.FragmentTvshowsBinding
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesViewPagerFragment : Fragment() {

    private var _binding: FragmentMoviesBinding? = null
    private lateinit var adapter: MoviesViewPagerAdapter

    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val list = listOf<String>("Most Popular", "Top Rated")
        adapter = MoviesViewPagerAdapter(list.size,this)
        binding.pager.adapter = adapter

        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = list[position]
        }.attach()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

class MoviesViewPagerAdapter (size: Int, fragment: Fragment): FragmentStateAdapter(fragment){

    private val size = size

    override fun getItemCount(): Int {
        return size
    }

    override fun createFragment(position: Int): Fragment {
        return MoviesRecyclerFragment.newInstance(position)
    }

}