package co.feliperivera.rappimovies.ui.tv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import co.feliperivera.rappimovies.R
import co.feliperivera.rappimovies.databinding.FragmentTvshowsBinding
import co.feliperivera.rappimovies.ui.movies.MoviesRecyclerFragment
import co.feliperivera.rappimovies.ui.movies.MoviesViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TVViewPagerFragment : Fragment() {

    private var _binding: FragmentTvshowsBinding? = null
    private lateinit var adapter: TVViewPagerAdapter

    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTvshowsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val list = listOf<String>(getString(R.string.most_popular), getString(R.string.top_rated))
        adapter = TVViewPagerAdapter(list.size,this)
        binding.pager.adapter = adapter

        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = list[position]
        }.attach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

class TVViewPagerAdapter (size: Int, fragment: Fragment): FragmentStateAdapter(fragment){

    private val size = size

    override fun getItemCount(): Int {
        return size
    }

    override fun createFragment(position: Int): Fragment {
        return TVRecyclerFragment.newInstance(position)
    }

}