package co.feliperivera.rappimovies.ui.movies

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import co.feliperivera.rappimovies.R
import co.feliperivera.rappimovies.databinding.ActivityMovieDetailsBinding
import co.feliperivera.rappimovies.viewmodels.MoviesViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity(), YouTubePlayer.OnInitializedListener  {

    private lateinit var binding: ActivityMovieDetailsBinding
    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"
    private var youtubeVideo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val movieModel : MoviesViewModel by viewModels()
        movieModel.movieId = intent.getIntExtra("movieId", 0)
        movieModel.getMovieDetails()
        binding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        movieModel.currentMovie.observe(this) { movie->
            Log.d("movie.id", movie.id.toString())
            if(movie.poster_path != null){
                val movieImagePath = IMAGE_BASE_URL + movie.poster_path
                Picasso.get()
                        .load(movieImagePath)
                        .fit()
                        .centerInside()
                        .into(binding.imageView2)
            }
            supportActionBar?.title = movie.title
            binding.overview.text = movie.overview
            binding.releaseDate.text = movie.release_date
            binding.originalTitle.text = movie.original_title
            binding.originalLanguage.text = movie.original_language
            binding.status.text = movie.status

            movieModel.getMovieVideos()


        }
        movieModel.videos.observe(this){ videos ->
            for(item in videos){
                if(item.site == "YouTube"){
                    youtubeVideo = item.key
                    val frag =
                        supportFragmentManager.findFragmentById(R.id.youtube_fragment) as YouTubePlayerSupportFragmentX?
                    frag!!.initialize("AIzaSyBzT7qi_kspmNWivji_Pw_jUw2ahEMltLY", this)
                    break
                }
            }
        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)

    }

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
        if (!p2) {
            if (p1 != null) {
                p1.cueVideo(youtubeVideo)
            }
        }

    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
        Snackbar.make(binding.root, getString(R.string.install_youtube), Snackbar.LENGTH_SHORT).show()
        Log.d("error", p1.toString())
    }
}