package co.feliperivera.rappimovies.ui.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import co.feliperivera.rappimovies.databinding.FragmentRecyclerBinding
import co.feliperivera.rappimovies.viewmodels.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesRecyclerFragment : Fragment() {

    private var position = 0

    private val localModel : MoviesViewModel by viewModels()

    private var _binding: FragmentRecyclerBinding? = null

    private val binding get() = _binding!!



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            position = it.getInt(ARG_POSITION)
        }
    }

    @ExperimentalPagingApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRecyclerBinding.inflate(inflater, container, false)

        with(binding.root) {
            layoutManager = LinearLayoutManager(context)
            val pagingAdapter = MovieRecyclerViewAdapter(MovieComparator)
            adapter = pagingAdapter
            if(position == 0){
                localModel.mostPopularMovies.observe(viewLifecycleOwner) { pagingData ->
                    pagingAdapter.submitData(
                        lifecycle,
                        pagingData
                    )
                }
            } else if(position == 1){
                localModel.topRatedMovies.observe(viewLifecycleOwner) { pagingData ->
                    pagingAdapter.submitData(
                        lifecycle,
                        pagingData
                    )
                }
            }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_POSITION = "position"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(position: Int) =
                MoviesRecyclerFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_POSITION, position)
                    }
                }
    }

}