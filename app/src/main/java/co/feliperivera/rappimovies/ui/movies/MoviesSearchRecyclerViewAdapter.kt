package co.feliperivera.rappimovies.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.databinding.ActivitySearchBinding
import co.feliperivera.rappimovies.databinding.ListLayoutBinding
import co.feliperivera.rappimovies.ui.movies.MoviesViewPagerFragmentDirections
import com.squareup.picasso.Picasso

class MoviesSearchRecyclerViewAdapter(
    private val values: List<Movie>
) : RecyclerView.Adapter<MoviesSearchRecyclerViewAdapter.ViewHolder>() {

    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: Movie = values[position]

        val movieImagePath = IMAGE_BASE_URL + item.poster_path
        holder.binding.itemNumber.text = (position + 1).toString()
        holder.binding.content.text = item.title
        Picasso.get()
            .load(movieImagePath)
            .fit()
            .centerInside()
            .into(holder.binding.imageView)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ListLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}
