package co.feliperivera.rappimovies.ui.tv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.databinding.ActivitySearchBinding
import co.feliperivera.rappimovies.databinding.ListLayoutBinding
import co.feliperivera.rappimovies.ui.movies.MoviesViewPagerFragmentDirections
import com.squareup.picasso.Picasso

class TVSearchRecyclerViewAdapter(
    private val values: List<TV>
) : RecyclerView.Adapter<TVSearchRecyclerViewAdapter.ViewHolder>() {

    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: TV = values[position]
        val movieImagePath = IMAGE_BASE_URL + item.poster_path
        holder.binding.itemNumber.text = (position + 1).toString()
        holder.binding.content.text = item.name
        Picasso.get()
            .load(movieImagePath)
            .fit()
            .centerInside()
            .into(holder.binding.imageView)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ListLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}
