package co.feliperivera.rappimovies.ui.tv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import co.feliperivera.rappimovies.databinding.FragmentRecyclerBinding
import co.feliperivera.rappimovies.viewmodels.TVViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class TVRecyclerFragment : Fragment() {

    private var position = 0

    private val localModel : TVViewModel by viewModels()

    private var _binding: FragmentRecyclerBinding? = null

    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            position = it.getInt(ARG_POSITION)
        }
    }

    @ExperimentalPagingApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRecyclerBinding.inflate(inflater, container, false)

        // Set the adapter

        with(binding.root) {
            layoutManager = LinearLayoutManager(context)
            val pagingAdapter = TVRecyclerViewAdapter(TVComparator)
            adapter = pagingAdapter
            if(position == 0){
                localModel.mostPopularTVSeries.observe(viewLifecycleOwner) { pagingData ->
                    pagingAdapter.submitData(
                        lifecycle,
                        pagingData
                    )
                }
            } else if(position == 1){
                localModel.topRatedTVSeries.observe(viewLifecycleOwner) { pagingData ->
                    pagingAdapter.submitData(
                            lifecycle,
                            pagingData
                    )
                }
            }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }



    companion object {

        // TODO: Customize parameter argument names
        const val ARG_POSITION = "position"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(position: Int) =
                TVRecyclerFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_POSITION, position)
                    }
                }
    }

}