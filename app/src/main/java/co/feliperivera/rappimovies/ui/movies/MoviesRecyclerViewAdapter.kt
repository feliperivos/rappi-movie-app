package co.feliperivera.rappimovies.ui.movies

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.get
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.databinding.ListLayoutBinding
import com.squareup.picasso.Picasso

class MovieRecyclerViewAdapter(
    diffCallback: DiffUtil.ItemCallback<Movie>
) : PagingDataAdapter<Movie, MovieRecyclerViewAdapter.ViewHolder>(diffCallback){

    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: Movie? = getItem(position)

        if (item != null) {
            val movieImagePath = IMAGE_BASE_URL + item.poster_path
            holder.binding.itemNumber.text = (position + 1).toString()
            holder.binding.content.text = item.title
            Picasso.get()
                .load(movieImagePath)
                .fit()
                .centerInside()
                .into(holder.binding.imageView)
            holder.binding.cardView.setOnClickListener {
                val action = MoviesViewPagerFragmentDirections.actionNavigationMoviesToMovieDetailsActivity(item.id)
                holder.binding.cardView.findNavController().navigate(action)
            }
        }
    }

    inner class ViewHolder(val binding: ListLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}

object MovieComparator : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        // Id is unique.
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}