package co.feliperivera.rappimovies.ui.tv

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import co.feliperivera.rappimovies.R
import co.feliperivera.rappimovies.databinding.ActivityMovieDetailsBinding
import co.feliperivera.rappimovies.databinding.ActivityTvDetailsBinding
import co.feliperivera.rappimovies.viewmodels.MoviesViewModel
import co.feliperivera.rappimovies.viewmodels.TVViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class TVDetailsActivity : AppCompatActivity(), YouTubePlayer.OnInitializedListener  {

    private lateinit var binding: ActivityTvDetailsBinding
    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185"
    private var youtubeVideo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val tvModel : TVViewModel by viewModels()
        tvModel.tvId = intent.getIntExtra("tvId", 0)
        tvModel.getTVDetails()
        binding = ActivityTvDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        tvModel.currentTV.observe(this) { tv->
            val tvImagePath = IMAGE_BASE_URL + tv.poster_path

            supportActionBar?.title = tv.name
            binding.overview.text = tv.overview
            binding.firstAirDate.text = tv.first_air_date
            binding.originalName.text = tv.original_name
            binding.originalLanguage.text = tv.original_language
            binding.status.text = tv.status

            tvModel.getTVVideos()

            Picasso.get()
                .load(tvImagePath)
                .fit()
                .centerInside()
                .into(binding.imageView2)
        }
        tvModel.videos.observe(this){ videos ->
            for(item in videos){
                if(item.site == "YouTube"){
                    youtubeVideo = item.key
                    val frag =
                        supportFragmentManager.findFragmentById(R.id.youtube_fragment) as YouTubePlayerSupportFragmentX?
                    frag!!.initialize("AIzaSyBzT7qi_kspmNWivji_Pw_jUw2ahEMltLY", this)
                    break
                }
            }
        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)

    }

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
        if (!p2) {
            if (p1 != null) {
                p1.cueVideo(youtubeVideo)
            }
        }

    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
        Snackbar.make(binding.root, getString(R.string.install_youtube), Snackbar.LENGTH_SHORT).show()
        Log.d("error", p1.toString())
    }
}