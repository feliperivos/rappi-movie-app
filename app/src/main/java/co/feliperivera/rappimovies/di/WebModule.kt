package co.feliperivera.rappimovies.di

import co.feliperivera.rappimovies.data.*
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.daos.RemoteKeysDao
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@InstallIn(SingletonComponent::class)
@Module
class WebModule {

    @Provides
    fun provideWebService(okHttpClient: OkHttpClient) : WebService {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(WebService::class.java)
    }

    @Provides
    fun provideAuthInterceptorOkHttpClient(
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                val originalHttpUrl = chain.request().url()
                val url =
                    originalHttpUrl.newBuilder().addQueryParameter("api_key", "11fd1064a95b26a0991178e2df0265b6")
                        .build()
                request.url(url)
                val response = chain.proceed(request.build())
                return@addInterceptor response
            }
            .build()
    }

    @PopularTVMediator
    @Provides
    fun providePopularTVMediator(tvDao: TVDao, remoteKeysDao: RemoteKeysDao, webService: WebService) : MyRemoteMediator<TV>{
        return MyRemoteMediator<TV>(ListType.POPULAR_TV, tvDao,remoteKeysDao,webService)
    }

    @TopRatedTVMediator
    @Provides
    fun provideTopRatedTVMediator(tvDao: TVDao, remoteKeysDao: RemoteKeysDao, webService: WebService) : MyRemoteMediator<TV>{
        return MyRemoteMediator<TV>(ListType.TOP_RATED_TV, tvDao,remoteKeysDao,webService)
    }

    @PopularMoviesMediator
    @Provides
    fun providePopularMoviesMediator(moviesDao: MovieDao, remoteKeysDao: RemoteKeysDao, webService: WebService) : MyRemoteMediator<Movie>{
        return MyRemoteMediator<Movie>(ListType.POPULAR_MOVIE, moviesDao,remoteKeysDao,webService)
    }

    @TopRatedMoviesMediator
    @Provides
    fun provideTopRatedMoviesMediator(moviesDao: MovieDao, remoteKeysDao: RemoteKeysDao, webService: WebService) : MyRemoteMediator<Movie>{
        return MyRemoteMediator<Movie>(ListType.TOP_RATED_MOVIE, moviesDao,remoteKeysDao,webService)
    }
}