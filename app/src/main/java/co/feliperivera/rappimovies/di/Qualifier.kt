package co.feliperivera.rappimovies.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PopularTVMediator

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class TopRatedTVMediator

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PopularMoviesMediator

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class TopRatedMoviesMediator