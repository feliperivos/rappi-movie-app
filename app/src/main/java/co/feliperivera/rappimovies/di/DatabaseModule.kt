package co.feliperivera.rappimovies.di

import android.content.Context
import androidx.room.Room
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.MyDatabase
import co.feliperivera.rappimovies.data.daos.RemoteKeysDao
import co.feliperivera.rappimovies.data.daos.TVDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context) : MyDatabase {
        return Room.databaseBuilder(
            appContext,
            MyDatabase::class.java,
            "rappimovies.db"
        ).build()
    }

    @Provides
    fun provideMovieDao(database: MyDatabase) : MovieDao {
        return database.movieDao()
    }

    @Provides
    fun provideTVDao(database: MyDatabase) : TVDao {
        return database.tvDao()
    }

    @Provides
    fun provideRemoteKeysDao(database: MyDatabase) : RemoteKeysDao {
        return database.remoteKeysDao()
    }

}