package co.feliperivera.rappimovies.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import co.feliperivera.rappimovies.data.*
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.repositories.MovieRepository
import co.feliperivera.rappimovies.di.PopularMoviesMediator
import co.feliperivera.rappimovies.di.TopRatedMoviesMediator
import co.feliperivera.rappimovies.utils.CoroutineContextProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


@HiltViewModel
class MoviesViewModel @Inject constructor(
        private val movieDao: MovieDao,
        private val movieRepository: MovieRepository,
        @PopularMoviesMediator private val popularMoviesMediator: MyRemoteMediator<Movie>,
        @TopRatedMoviesMediator private val topRatedMoviesMediator: MyRemoteMediator<Movie>,
        private val contextProvider: CoroutineContextProvider
        ) : ViewModel() {

    val ioContext: CoroutineContext = (contextProvider.IO )

    var movieId: Int = 0

    val currentMovie: LiveData<Movie> by lazy {
        movieDao.getMovieDetails(movieId)
    }

    val videos: MutableLiveData<List<Videos>> by lazy {
        MutableLiveData<List<Videos>>()
    }

    @ExperimentalPagingApi
    var mostPopularMovies: LiveData<PagingData<Movie>> = Pager(
        PagingConfig( 20),

        null,  // initialKey
        popularMoviesMediator,
        { movieDao.getMostPopularMovies() }
    ).liveData

    @ExperimentalPagingApi
    var topRatedMovies: LiveData<PagingData<Movie>> = Pager(
            PagingConfig( 20),
            null,  // initialKey
            topRatedMoviesMediator,
            { movieDao.getTopRatedMovies() }
    ).liveData

    fun getMovieDetails() {
        viewModelScope.launch(ioContext) {
            movieRepository.getMovieDetails(movieId)
        }
    }

    fun getMovieVideos() {
        viewModelScope.launch(ioContext) {
            videos.postValue(movieRepository.getMovieVideoLink(movieId))
        }
    }


}