package co.feliperivera.rappimovies.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.feliperivera.rappimovies.data.repositories.SearchRepository
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.utils.CoroutineContextProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class SearchViewModel @Inject constructor(
        private val searchRepository: SearchRepository,
        private val contextProvider: CoroutineContextProvider
) : ViewModel() {

    val ioContext: CoroutineContext = (contextProvider.IO )

    var query: String = ""

    val movies: MutableLiveData<List<Movie>> by lazy {
        MutableLiveData<List<Movie>>()
    }

    val tvShows: MutableLiveData<List<TV>> by lazy {
        MutableLiveData<List<TV>>()
    }

    fun findMovies() {
        viewModelScope.launch(ioContext) {
            movies.postValue(searchRepository.searchMovies(query))
        }
    }

    fun findTVShows() {
        viewModelScope.launch(ioContext) {
            tvShows.postValue(searchRepository.searchTV(query))
        }
    }



}