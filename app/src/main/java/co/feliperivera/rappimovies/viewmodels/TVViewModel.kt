package co.feliperivera.rappimovies.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import co.feliperivera.rappimovies.data.*
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.data.repositories.TVRepository
import co.feliperivera.rappimovies.di.PopularTVMediator
import co.feliperivera.rappimovies.di.TopRatedTVMediator
import co.feliperivera.rappimovies.utils.CoroutineContextProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


@HiltViewModel
class TVViewModel @Inject constructor(
        private val tvDao: TVDao,
        private val tvRepository: TVRepository,
        @PopularTVMediator private val popularTvMediator: MyRemoteMediator<TV>,
        @TopRatedTVMediator private val topRatedTVMediator: MyRemoteMediator<TV>,
        private val contextProvider: CoroutineContextProvider
        ) : ViewModel() {

    val ioContext: CoroutineContext = (contextProvider.IO )

    var tvId: Int = 0

    val currentTV: LiveData<TV> by lazy {
        tvDao.getTVDetails(tvId)
    }

    val videos: MutableLiveData<List<Videos>> by lazy {
        MutableLiveData<List<Videos>>()
    }



    @ExperimentalPagingApi
    var mostPopularTVSeries: LiveData<PagingData<TV>> = Pager(
        PagingConfig( 20),

        null,  // initialKey
        popularTvMediator,
        { tvDao.getMostPopularTVSeries() }
    ).liveData

    @ExperimentalPagingApi
    var topRatedTVSeries: LiveData<PagingData<TV>> = Pager(
            PagingConfig( 20),
            null,  // initialKey
            topRatedTVMediator,
            { tvDao.getTopRatedTVSeries() }
    ).liveData

    fun getTVDetails() {
        viewModelScope.launch(ioContext) {
            tvRepository.getTVDetails(tvId)
        }
    }

    fun getTVVideos() {
        viewModelScope.launch(ioContext) {
            videos.postValue(tvRepository.getTVVideoLink(tvId))
        }
    }


}