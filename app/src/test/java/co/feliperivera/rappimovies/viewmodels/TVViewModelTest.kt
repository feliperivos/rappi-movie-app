package co.feliperivera.rappimovies.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.feliperivera.rappimovies.TestContextProvider
import co.feliperivera.rappimovies.data.MyRemoteMediator
import co.feliperivera.rappimovies.data.Videos
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.data.repositories.SearchRepository
import co.feliperivera.rappimovies.data.repositories.TVRepository
import com.google.common.truth.Truth
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mockito
import java.util.ArrayList

class TVViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private lateinit var SUT: TVViewModel
    private lateinit var testContextProvider: TestContextProvider
    private lateinit var repository: TVRepository
    private lateinit var tvDao: TVDao
    private lateinit var popularTvMediator: MyRemoteMediator<TV>
    private lateinit var topRatedTVMediator: MyRemoteMediator<TV>
    private val singleVideo = Videos(key = "eausoet", site = "YouTube")

    @Before
    fun setUp() {
        repository = Mockito.mock(TVRepository::class.java)
        tvDao = Mockito.mock(TVDao::class.java)
        popularTvMediator = Mockito.mock(MyRemoteMediator::class.java) as MyRemoteMediator<TV>
        topRatedTVMediator = Mockito.mock(MyRemoteMediator::class.java) as MyRemoteMediator<TV>
        testContextProvider = TestContextProvider()
        SUT = TVViewModel(tvDao,repository,popularTvMediator,topRatedTVMediator,testContextProvider)
    }

    @Test
    fun getTVDetails() = runBlockingTest{
        SUT.getTVDetails()
        Mockito.verify(repository).getTVDetails(SUT.tvId)
    }

    @Test
    fun getTVVideos() = runBlockingTest{
        SUT.getTVVideos()
        Mockito.verify(repository).getTVVideoLink(SUT.tvId)
    }

    @Test
    fun getTVVideosSetsVideos() = runBlockingTest{
        returnListVideos()
        var list = ArrayList<Videos>()
        list.add(singleVideo)
        SUT.getTVVideos()
        Truth.assertThat(SUT.videos.value).isEqualTo(list)
    }

    private fun returnListVideos() = runBlockingTest {
        var list = ArrayList<Videos>()
        list.add(singleVideo)
        Mockito.`when`(repository.getTVVideoLink(SUT.tvId)).thenReturn(list)
    }
}