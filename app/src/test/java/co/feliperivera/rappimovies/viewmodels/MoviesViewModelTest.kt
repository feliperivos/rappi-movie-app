package co.feliperivera.rappimovies.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.feliperivera.rappimovies.TestContextProvider
import co.feliperivera.rappimovies.data.MyRemoteMediator
import co.feliperivera.rappimovies.data.Videos
import co.feliperivera.rappimovies.data.daos.MovieDao
import co.feliperivera.rappimovies.data.daos.TVDao
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.data.repositories.MovieRepository
import co.feliperivera.rappimovies.data.repositories.TVRepository
import co.feliperivera.rappimovies.di.PopularMoviesMediator
import co.feliperivera.rappimovies.di.TopRatedMoviesMediator
import com.google.common.truth.Truth
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mockito
import java.util.ArrayList

class MoviesViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private lateinit var SUT: MoviesViewModel
    private lateinit var testContextProvider: TestContextProvider
    private lateinit var repository: MovieRepository
    private lateinit var movieDao: MovieDao
    private lateinit var popularMoviesMediator: MyRemoteMediator<Movie>
    private lateinit var topRatedMoviesMediator: MyRemoteMediator<Movie>
    private val singleVideo = Videos(key = "eausoet", site = "YouTube")

    @Before
    fun setUp() {
        repository = Mockito.mock(MovieRepository::class.java)
        movieDao = Mockito.mock(MovieDao::class.java)
        popularMoviesMediator = Mockito.mock(MyRemoteMediator::class.java) as MyRemoteMediator<Movie>
        topRatedMoviesMediator = Mockito.mock(MyRemoteMediator::class.java) as MyRemoteMediator<Movie>
        testContextProvider = TestContextProvider()
        SUT = MoviesViewModel(movieDao,repository,popularMoviesMediator,topRatedMoviesMediator,testContextProvider)
    }

    @Test
    fun getMovieDetails() = runBlockingTest{
        SUT.getMovieDetails()
        Mockito.verify(repository).getMovieDetails(SUT.movieId)
    }

    @Test
    fun getMovieVideos() = runBlockingTest{
        SUT.getMovieVideos()
        Mockito.verify(repository).getMovieVideoLink(SUT.movieId)
    }

    @Test
    fun getTVVideosSetsVideos() = runBlockingTest{
        returnListVideos()
        var list = ArrayList<Videos>()
        list.add(singleVideo)
        SUT.getMovieVideos()
        Truth.assertThat(SUT.videos.value).isEqualTo(list)
    }

    private fun returnListVideos() = runBlockingTest {
        var list = ArrayList<Videos>()
        list.add(singleVideo)
        Mockito.`when`(repository.getMovieVideoLink(SUT.movieId)).thenReturn(list)
    }
}