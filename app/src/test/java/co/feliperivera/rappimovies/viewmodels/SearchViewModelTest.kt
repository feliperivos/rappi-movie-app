package co.feliperivera.rappimovies.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.feliperivera.rappimovies.TestContextProvider
import co.feliperivera.rappimovies.data.entities.Movie
import co.feliperivera.rappimovies.data.entities.TV
import co.feliperivera.rappimovies.data.repositories.SearchRepository
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mockito
import org.mockito.Mockito.verify
import java.util.*
import com.google.common.truth.Truth.assertThat

class SearchViewModelTest {

    @get:Rule
    val instantTestExecutorRule = InstantTaskExecutorRule()

    private lateinit var SUT: SearchViewModel
    private lateinit var testContextProvider: TestContextProvider
    private lateinit var repository: SearchRepository
    private val singleMovie = Movie(
            title = "Title",
            popularity = 134f,
            vote_average = 313f,
            original_language = null,
            original_title = null,
            release_date = null,
            poster_path = null,
            overview = null,
            backdrop_path = null,
            status = null)
    private val singleTV = TV(
            name = "Title",
            popularity = 134f,
            vote_average = 313f,
            original_language = null,
            original_name = "Title",
            poster_path = null,
            overview = "aoeu",
            backdrop_path = null,
            status = null,
            first_air_date = null)

    @Before
    fun setUp() {
        repository = Mockito.mock(SearchRepository::class.java)
        testContextProvider = TestContextProvider()
        SUT = SearchViewModel(repository, testContextProvider)
    }

    @Test
    fun findMoviesCallRepositorySearchMovies() = runBlockingTest{
        SUT.findMovies()
        verify(repository).searchMovies(SUT.query)
    }

    @Test
    fun findMoviesSetsMovieList() = runBlockingTest{
        returnListMovie()
        var list = ArrayList<Movie>()
        list.add(singleMovie)
        SUT.findMovies()
        assertThat(SUT.movies.value).isEqualTo(list)
    }

    @Test
    fun findTVShowsCallRepositorySearchTV() = runBlockingTest{
        SUT.findTVShows()
        verify(repository).searchTV(SUT.query)
    }

    @Test
    fun findTVShowsSetsTVList() = runBlockingTest{
        returnListTV()
        var list = ArrayList<TV>()
        list.add(singleTV)
        SUT.findTVShows()
        assertThat(SUT.tvShows.value).isEqualTo(list)
    }

    private fun returnListMovie() = runBlockingTest {
        var list = ArrayList<Movie>()
        list.add(singleMovie)
        Mockito.`when`(repository.searchMovies(SUT.query)).thenReturn(list)
    }

    private fun returnListTV() = runBlockingTest {
        var list = ArrayList<TV>()
        list.add(singleTV)
        Mockito.`when`(repository.searchTV(SUT.query)).thenReturn(list)
    }


}